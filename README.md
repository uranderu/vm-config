# VM-Config
## What is this?
This is a small shell script that installs and configures packages and settings that I always use on VMs and LXC Containers that I deploy on my Homelab Hypervisor.
## What does it do?
It does a few things:
- Prompts to update the entire system, afterwards it reboots.
- Sets the timezone to Europe/Amsterdam
- Sets up UFW (Firewall) with SSH access.
- Installs and sets up automatic upgrades with the config that I always use.
- Prompts for installation of the QEMU Guest Agent.
- Installs and configures SSH brute force protection.
## Can I use it and/or contribute?
This script was specifically made for Ubuntu servers. It uses the apt package manager when updating/installing updates.
Other distro's like CentOS or RHEL don't use this package manager, so it isn't of much use on those systems.
I am always open for contributions or improvements. Create an issue or fork the repo and code it yourself!
