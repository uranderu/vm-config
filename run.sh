#!/bin/bash

# Run as root prompt
if [ "$EUID" -ne 0 ]
  then echo "Please run this script as root!"
  exit
fi

# Update prompt
echo "Make sure your system is updated. Do you want to update this system right now?"
while true; do
    read -rp "(Y/n) " yn
    case $yn in
        [Yy]* ) apt update && apt upgrade -y && reboot -h now; break;;
        [Nn]* ) break;;
        * ) echo "Please answer yes or no.";;
    esac
done

# Set timezone
echo "Setting Timezone to Europe/Amsterdam."
timedatectl set-timezone Europe/Amsterdam

# Firewall installation and configuration
echo "Installing and configuring firewall."
apt install ufw -y
ufw allow ssh
ufw enable

# Automatic upgrades
echo "Installing unattended upgrades for automatic updates."
apt install unattended-upgrades -y

echo "Configuring unattended-upgrades"
AUTO_PATH=/etc/apt/apt.conf.d/20auto-upgrades
AUTO_PATH_BAK=/etc/apt/apt.conf.d/20auto-upgrades.bak

UNATTENDED_PATH=/etc/apt/apt.conf.d/50unattended-upgrades
UNATTENDED_PATH_BAK=/etc/apt/apt.conf.d/50unattended-upgrades.bak

mv $AUTO_PATH $AUTO_PATH_BAK && mv $UNATTENDED_PATH $UNATTENDED_PATH_BAK
cp ./var/auto-upgrades/20auto-upgrades $AUTO_PATH && cp ./var/auto-upgrades/50unattended-upgrades $UNATTENDED_PATH
chmod 644 $AUTO_PATH $UNATTENDED_PATH

# VM specific
echo "Is this a VM? Answering yes will install and configure VM specific software."
while true; do
    read -rp "(Y/n) " yn
    case $yn in
        [Yy]* ) apt install qemu-guest-agent; apt install fail2ban -y; systemctl enable fail2ban.service; cp ./var/ssh-protection/jail.local /etc/fail2ban/jail.local; systemctl restart fail2ban.service; echo "Script ran successfully."; exit 0; break;;
        [Nn]* ) break;;
        * ) echo "Please answer yes or no.";;
    esac
done

# SSH brute force protection
apt install sshguard -y

systemctl start sshguard
systemctl enable sshguard

UFW_PATH=/etc/ufw/before.rules
UFW_PATH_BAK=/etc/ufw/before.rules.bak

mv $UFW_PATH $UFW_PATH_BAK
cp ./var/ssh-protection/before.rules $UFW_PATH

systemctl restart ufw

echo "Script ran successfully."
